// DOM elements to modify
const elBalance = document.querySelector('#balance');
const elOutstandingLoan = document.querySelector('#outstandingLoan');
const elOLVisibility = document.querySelectorAll('.hidden');

const elPay = document.querySelector('#pay');

const elLaptops = document.querySelector('#laptops');
const elFeatureList = document.querySelector('#featureList');
const elLaptopName = document.querySelector('#laptopName');
const elLaptopDesc = document.querySelector('#laptopDesc');
const elLaptopPrice = document.querySelector('#laptopPrice');
const elInfo = document.querySelector('#info');
const elImgContainer = document.querySelector('.imgContainer');

const elRepayLoanBtn = document.querySelector('#repayLoanBtn');
const elLoanBtn = document.querySelector('#loanBtn');
const elBankBtn = document.querySelector('#bankBtn');
const elWorkBtn = document.querySelector('#workBtn');
const elBuyBtn = document.querySelector('#buyBtn');

// Other variables
let balance = 0;
let outstandingLoan = 0;
let pay = 0;
let computers = [];
let selectedLaptop = {};

// API
const baseUrl = "https://noroff-komputer-store-api.herokuapp.com";

// Event listeners
elRepayLoanBtn.addEventListener("click", handleRepayLoan);
elLoanBtn.addEventListener("click", handleLoan);
elBankBtn.addEventListener("click", handleTransfer);
elWorkBtn.addEventListener("click", handleWork);
elBuyBtn.addEventListener("click", handleBuy);
elLaptops.addEventListener("change", handleLaptopSelection);


// ----------- BANK ------------------

/* Function that handles loans and corresponding constraints. Uses helper function to toggle visibility of conditionally hidden elements. */
function handleLoan() {
  
  if(outstandingLoan !== 0 && outstandingLoan !== null) {
    alert("Please repay your outstanding debts before applying for a new loan.");
    return;
  }
  
  const amountToLoan = parseInt(prompt("Enter the amount you wish to loan:"));

  if((amountToLoan <= balance * 2)) {
    outstandingLoan = amountToLoan;
    balance += amountToLoan;
    elOutstandingLoan.innerText = outstandingLoan;
    elBalance.innerText = balance;
  } else if(amountToLoan > balance * 2) {
    alert("You cannot loan an amount this high. Back to work!");
  } else if(amountToLoan <= 0) {
    alert("Invalid value")
  }
  toggleOLVisibility();
}

// ----------- WORK -----------------
/* Function that handles pay from work */
function handleWork() {
  pay += 100;
  elPay.innerText = pay;
}

/* Function that handles transfer from "wallet" to bank
with constraints regarding outstanding loan. Helper function toggles visibility of conditionally hidden elements*/
function handleTransfer() {
  if(outstandingLoan > 0) {
    outstandingLoan -= pay * 0.1;
    balance += pay * 0.9;
    pay = 0; 

    elOutstandingLoan.innerText = outstandingLoan;
  } else {
    balance += pay;
    pay = 0;
  }
  elBalance.innerText = balance;
  elPay.innerText = pay;
  toggleOLVisibility();
}

/* function that handles repayment of loan. Helper function toggles visibility of conditionally hidden elements */
function handleRepayLoan() {
  if(pay > outstandingLoan) {
    pay = pay - outstandingLoan;
    outstandingLoan -= outstandingLoan;
  } else {
    outstandingLoan -= pay;
    pay = 0;
  }
  elOutstandingLoan.innerText = outstandingLoan;
  elPay.innerText = `${pay} kr`;
  toggleOLVisibility();
}

// --------- LAPTOP -----------------
/* Asynchronous function to fetch data from API. 
@return computersRes <- results from API call */
async function fetchComputers() {
  const res = await fetch(`${baseUrl}/computers`);
  if(!res.ok) {
    throw new Error("Something went wrong while fetching computers.")
  }

  const computersRes = await res.json();

  computers = computersRes;
  computersRes.forEach(computer => addComputer(computer));
  return computersRes;
}
/*function that adds computers to the select element
@param computer <- a single computer object from the API */
function addComputer(computer) {
  const elComputer = document.createElement("option");
  elComputer.innerText = computer.title;
  elComputer.value = computer.id;
  elComputer.id = computer.id; 
  elLaptops.appendChild(elComputer);
}
/* function that displays more information about the chosen laptop
@param e <- event we listened for with eventlistener */
function handleLaptopSelection(e) {
  if(!e.target.value) return elFeatureList.innerHTML= ""; 
  selectedLaptop = computers.find(item => item.id === parseInt(e.target.value));

  elFeatureList.innerHTML= "";
  selectedLaptop.specs.forEach(spec => {
    const elSpec = document.createElement('li');
    elSpec.innerText = spec;
    elFeatureList.appendChild(elSpec)
  })
  getImage(selectedLaptop);
  elLaptopName.innerText = selectedLaptop.title;
  elLaptopDesc.innerText = selectedLaptop.description;
  elLaptopPrice.innerText = `${selectedLaptop.price} kr`;
}
/*function that adds an image to the dom. Fallback-picture in case of error with the data.
@param laptop <- a single laptop object */
function getImage(laptop) {
  elImgContainer.innerHTML = '';
  const image = document.createElement('img');
  image.src = `${baseUrl}/${laptop.image}`;
  image.alt = `${laptop.title}`;
  image.onerror = () => {
    image.src = "https://http.cat/404";
    image.alt = "Placeholder image";
  }
  elImgContainer.appendChild(image);
}
/*function that buys a PC if the user have enough in the bank balance */
function handleBuy() {
  let price = selectedLaptop.price;
  if(price <= balance) {
    balance -= price;
    elBalance.innerText = balance;
    alert(`Congratulations with your "new" laptop: ${selectedLaptop.title}`);
  } else {
    alert("You have insufficient funds for this purchase.");
  }
}

// ----- ADDITIONAL FUNCTIONS -------
/* Function toggles visibility for elements of class outstandingLoan 
returns nothing*/
function toggleOLVisibility() {
  if(outstandingLoan === 0 || outstandingLoan === null) {
    for(let el of elOLVisibility) {
      el.classList.add('hidden');
    }
  } else if(outstandingLoan > 0) {
    for(let el of elOLVisibility) {
      el.classList.remove('hidden');
    }
  }

}
// running fetch, since the function is not called by any other method/event.
fetchComputers();
